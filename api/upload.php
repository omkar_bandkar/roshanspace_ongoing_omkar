<?php
$ip = "http://localhost/";
$dir_path = "roshanspace_ongoing/uploads/";
$send_path =$ip.$dir_path;

$target_dir = "../uploads/";
$fileToUpload = $_FILES["fileToUpload"];

//edit file name
$temp = explode(".",$fileToUpload["name"]);
$newfilename = uniqid() . '.' .end($temp);
$target_file = $target_dir . $newfilename;
$send_path.=$newfilename;
$uploadOk = 1;


$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) {
    $check = getimagesize($fileToUpload["tmp_name"]);
    if($check !== false) {
        echo "File is an image - " . $check["mime"] . ".";
        $uploadOk = 1;
    } else {
        echo "File is not an image.";
        $uploadOk = 0;
    }
}
// Check if file already exists
if (file_exists($target_file)) {
    echo "Sorry, file already exists.";
    $uploadOk = 0;
}
// Check file size
if ($fileToUpload["size"] > 500000) {
    echo "Sorry, your file is too large.";
    $uploadOk = 0;
}
// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" ) {
    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($fileToUpload["tmp_name"], $target_file)) {
        $msg = "The file ". basename( $fileToUpload["name"]). " has been uploaded.";
        $file_path = $target_file;
        echo '{"status":"success","message":"'.$msg.'","file_path":"'.$send_path.'"}';
    } else {
        $msg = "Sorry, there was an error uploading your file.";
         echo '{"status":"error","message":"'.$msg.'"}';
    }
}
?>