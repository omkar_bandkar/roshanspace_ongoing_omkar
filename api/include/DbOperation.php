<?php

class DbOperation
{
    //Database connection link
    private $con;

    //Class constructor
    function __construct()
    {
        //Getting the DbConnect.php file
        require_once dirname(__FILE__) . '/DbConnect.php';

        //Creating a DbConnect object to connect to the database
        $db = new DbConnect();

        //Initializing our connection link of this class
        //by calling the method connect of DbConnect class
        $this->con = $db->connect();
    }


    //method to create Master Admin
    public function createMasterAdmin($request){
        $first_name = mysql_real_escape_string($request->getParam('first_name'));
        $last_name = mysql_real_escape_string($request->getParam('last_name'));
        $email_id = $request->getParam('email_id');
        $password = $request->getParam('password');         
        $hashed_password = password_hash($password, PASSWORD_DEFAULT);
        $contact_number = $request->getParam('contact_number'); // by key
        $address = mysql_real_escape_string($request->getParam('address'));
        $created_at = date("Y-m-d H:i:s"); 

        $data = $request->getParams(); // all data from query string or post body
        $responseData = array();
        if($first_name == "" || $last_name == "" || $contact_number == "" || $address == "" || $email_id == "" || $password == ""){      
            $responseData['status'] = "error";
            $responseData['message'] = "Some fields are missing";
            echo json_encode($responseData);
        }else{
            if(!$this->isExists($email_id, 'master_admin')) {            
                $id = uniqid('ma_');
                $query = "INSERT INTO master_admin(id, first_name, last_name, email_id, password, contact_number, address, created_at) VALUES ('$id', '$first_name', '$last_name', '$email_id', '$hashed_password', '$contact_number', '$address', '$created_at')";
                //Crating an statement
                $stmt = $this->con->prepare($query);
                //Executing the statment
                $result = $stmt->execute();
                //Closing the statment
                $this->con = null;
                if ($result) {
                //Insert success
                    $responseData['status'] = "success";
                    $responseData['message'] = "Success! account with Email id: ".$email_id." created";
                    echo json_encode($responseData);
                } else {
                //Insert fail
                    $responseData['status'] = "error";
                    $responseData['message'] = "Error! Something went wrong";
                    echo json_encode($responseData);
                }
            }else{
                $responseData['status'] = "error";
                $responseData['message'] = "Error! user already exist.";
                echo json_encode($responseData);
            }        
        }
    }

    //method to create Administrator
    public function createAdministrator($request){
        $first_name = mysql_real_escape_string($request->getParam('first_name'));
        $last_name = mysql_real_escape_string($request->getParam('last_name'));
        $email_id = $request->getParam('email_id');
        $password = $request->getParam('password');         
        $hashed_password = password_hash($password, PASSWORD_DEFAULT);
        $contact_number = $request->getParam('contact_number'); // by key
        $address = mysql_real_escape_string($request->getParam('address'));
        $created_at = date("Y-m-d H:i:s"); 

        $data = $request->getParams(); // all data from query string or post body
        $responseData = array();
        if($first_name == "" || $last_name == "" || $contact_number == "" || $address == "" || $email_id == "" || $password == ""){      
            $responseData['status'] = "error";
            $responseData['message'] = "Some fields are missing";
            echo json_encode($responseData);
        }else{
            if(!$this->isExists($email_id,'administrator')) {            
                $id = uniqid('ad_');
                $query = "INSERT INTO administrator(id, first_name, last_name, email_id, password, contact_number, address, created_at) VALUES ('$id', '$first_name', '$last_name', '$email_id', '$hashed_password', '$contact_number', '$address', '$created_at')";
                //Crating an statement
                $stmt = $this->con->prepare($query);
                //Executing the statment
                $result = $stmt->execute();
                //Closing the statment
                $this->con = null;
                if ($result) {
                    //Insert success
                    $responseData['status'] = "success";
                    $responseData['message'] = "Success! account with Email id: ".$email_id." created";
                    echo json_encode($responseData);
                } else {
                    //Insert fail
                    $responseData['status'] = "error";
                    $responseData['message'] = "Error! Something went wrong";
                    echo json_encode($responseData);
                }
            }else{
                $responseData['status'] = "error";
                $responseData['message'] = "Error! user already exist.";
                echo json_encode($responseData);
            }        
        }
    }

    //method to create Sales Person
    public function createSalesPerson($request){
        $first_name = mysql_real_escape_string($request->getParam('first_name'));
        $last_name = mysql_real_escape_string($request->getParam('last_name'));
        $email_id = $request->getParam('email_id');
        $password = $request->getParam('password');         
        $hashed_password = password_hash($password, PASSWORD_DEFAULT);
        $contact_number = $request->getParam('contact_number'); // by key
        $address = mysql_real_escape_string($request->getParam('address'));
        $joining_date = $request->getParam('joining_date');
        $leaving_date = $request->getParam('leaving_date');
        $created_at = date("Y-m-d H:i:s"); 

        $data = $request->getParams(); // all data from query string or post body
        $responseData = array();
        if($first_name == "" || $last_name == "" || $contact_number == "" || $address == "" || $email_id == "" || $password == ""){      
            $responseData['status'] = "error";
            $responseData['message'] = "Some fields are missing";
            echo json_encode($responseData);            
        }else{
            if(!$this->isExists($email_id,'sales_person')) {            
                $id = uniqid('sp_');
                $query = "INSERT INTO sales_person(id, first_name, last_name, email_id, password, contact_number, address, joining_date, leaving_date, created_at) VALUES ('$id', '$first_name', '$last_name', '$email_id', '$hashed_password', '$contact_number', '$address', '$joining_date', '$leaving_date', '$created_at')";
                //Crating an statement
                $stmt = $this->con->prepare($query);
                //Executing the statment
                $result = $stmt->execute();
                //Closing the statment
                $this->con = null;
                if ($result) {
                //Insert success
                    $responseData['status'] = "success";
                    $responseData['message'] = "Success! account with Email id: ".$email_id." created";
                    echo json_encode($responseData);
                } else {
                //Insert fail
                    $responseData['status'] = "error";
                    $responseData['message'] = "Error! Something went wrong";
                    echo json_encode($responseData);
                }
            }else{
                $responseData['status'] = "error";
                $responseData['message'] = "Error! user already exist.";
                echo json_encode($responseData);
            }        
        }
    }

    //method to create site
    public function createSite($request){
        $site_code = mysql_real_escape_string($request->getParam('site_code'));
        $location_name = mysql_real_escape_string($request->getParam('location_name'));
        $size = mysql_real_escape_string($request->getParam('size'));
        $cart_rate = mysql_real_escape_string($request->getParam('cart_rate'));
        $min_cart_rate = mysql_real_escape_string($request->getParam('min_cart_rate'));
        $landmark = mysql_real_escape_string($request->getParam('landmark'));

        $query = "SELECT id FROM site_info WHERE site_code = '$site_code' ";
        $stmt = $this->con->prepare($query);
        //Executing the statment
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if(count($result)){
            $responseData['status'] = "error";
            $responseData['message'] = "Error! Site with same sitecode already exist.";
        } else {
            $id = uniqid('st_');
            $created_at = date("Y-m-d H:i:s"); 
            $query = "INSERT INTO site_info(id, site_code, location_name, size, cart_rate, min_cart_rate, landmark, created_at) VALUES ('$id', '$site_code', '$location_name', '$size', '$cart_rate', '$min_cart_rate', '$landmark','$created_at')";
            //Crating an statement
            $stmt = $this->con->prepare($query);
            //Executing the statment
            $result = $stmt->execute();
            //Closing the statment
            $this->con = null;
            if ($result) {
            //Insert success
                $responseData['status'] = "success";
                $responseData['message'] = "Success! Site added Successfully.";
            } else {
            //Insert fail
                $responseData['status'] = "error";
                $responseData['message'] = "Error! Something went wrong Please refresh and try again.";
            }
        }
        echo json_encode($responseData);
    }

    //method to create Company
    public function createCompany($request){
        $party_name = mysql_real_escape_string($request->getParam('partyname'));
        $email_id = $request->getParam('email_id');
        $office_number = $request->getParam('ofcnumber');
        $other_number = $request->getParam('othernumber');
        $pan_number = $request->getParam('pannumber');
        $address = mysql_real_escape_string($request->getParam('address'));
        $created_at = date("Y-m-d H:i:s"); 

        $data = $request->getParams(); // all data from query string or post body
        $responseData = array();
        if($party_name == "" || $email_id == "" || $office_number == "" || $address == "" || $pan_number == ""){      
            $responseData['status'] = "error";
            $responseData['message'] = "Some fields are missing";
            echo json_encode($responseData);            
        }else{
            if(!$this->isExists($email_id,'customer')) {            
                $id = uniqid('cust_');
                $query = "INSERT INTO companies(id, party_name, email_id, address, office_number, other_number, PAN_number, created_at) VALUES ('$id', '$party_name', '$email_id', '$address', '$office_number', '$other_number', '$pan_number', '$created_at')";
                //Crating an statement
                $stmt = $this->con->prepare($query);
                //Executing the statment
                $result = $stmt->execute();
                //Closing the statment
                $this->con = null;
                if ($result) {
                //Insert success
                    $responseData['status'] = "success";
                    $responseData['message'] = "Success! Company added Successfully.";
                    echo json_encode($responseData);
                } else {
                //Insert fail
                    $responseData['status'] = "error";
                    $responseData['message'] = "Error! Something went wrong Please refresh and try again.";
                    echo json_encode($responseData);
                }
            }else{
                $responseData['status'] = "error";
                $responseData['message'] = "Error! Company with same email id already exist.";
                echo json_encode($responseData);
            }        
        }
    }



    //Checking whether a student already exist
    private function isExists($email_id, $type) {
        $query = null;
        switch ($type) {
            case 'master_admin':            
                $query = "SELECT id from master_admin WHERE email_id = '$email_id'";
            break;
            case 'administrator':
                $query = "SELECT id from administrator WHERE email_id = '$email_id'";
            break;
            case 'sales_person':
                $query = "SELECT id from sales_person WHERE email_id = '$email_id'";
            break;
            case 'customer':
                $query = "SELECT id from companies WHERE email_id = '$email_id'";
            break;
        }
        //Crating an statement
        $stmt = $this->con->prepare($query);
        //Executing the statment
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $num_rows = count($result);
        return $num_rows > 0;
    }

    //This method will generate a unique api key
    private function generateApiKey(){
        return md5(uniqid(rand(), true));
    }

    public function getAllCompanies($request){
        $query = "SELECT `id`,`party_name`,`address`,`office_number`,`other_number`,`PAN_number`,`email_id`,date(`created_at`) datecreated FROM `companies` ";
        //Crating an statement
        $stmt = $this->con->prepare($query);
        //Executing the statment
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $responseData = array();
        if(count($result)){
            $responseData['status'] = "success";
        }else{
            $responseData['status'] = "error";
            $responseData['message'] = "Fail to get customer List/ Customers list not available.";
        }
        $responseData['list'] = $result;
        echo json_encode($responseData);
        //Closing the statment
        $this->con = null;
        exit();
    }
    public function getPendingPOorders($request){
        $query = "select t1.id orderid,t3.id customerid,t3.party_name,t4.id salespersonid,CONCAT(t4.first_name,'',t4.last_name) salesperson,t5.id site_id, t5.location_name ,t5.site_code from order_info t1 inner join companies t3 on t3.id = t1.customer_id inner join sales_person t4 on t4.id = t1.sales_person_id inner join site_info t5 on t5.id = t1.site_id left join po_reveived t2 on t1.id = t2.order_id where t2.id is null";
        $stmt = $this->con->prepare($query);
        //Executing the statment
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $responseData = array();
        if(count($result)){
            $responseData['status'] = "success";
        }else{
            $responseData['status'] = "error";
            $responseData['message'] = "Fail to get Pending PO Orders/ Pending Orders not available.";
        }
        $responseData['list'] = $result;
        echo json_encode($responseData);
        //Closing the statment
        $this->con = null;
        exit();
    }
    public function getAvailableSites($request){
        $dateSelected = mysql_real_escape_string($request->getAttribute('date'));
        $query = "select t1.id,t1.location_name,t1.site_code,t1.landmark from site_info t1 left join order_info t2 on t2.site_id = t1.id where ( '$dateSelected' NOT between t2.from_date and t2.to_date) or ( t2.id is null)";
        $stmt = $this->con->prepare($query);
        //Executing the statment
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $responseData = array();
        if(count($result)){
            $responseData['status'] = "success";
        }else{
            $responseData['status'] = "error";
            $responseData['message'] = "Fail to get Available Sites/ No sites available.";
        }
        $responseData['list'] = $result;
        echo json_encode($responseData);
        //Closing the statment
        $this->con = null;
        exit();
    }
    public function getBookedSites($request){
        $dateSelected = mysql_real_escape_string($request->getAttribute('date'));
        $query = "select t1.id,t1.location_name,t1.site_code,t1.landmark from site_info t1 inner join order_info t2 on t2.site_id = t1.id where ( '$dateSelected' between t2.from_date and t2.to_date)";
        $stmt = $this->con->prepare($query);
        //Executing the statment
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $responseData = array();
        if(count($result)){
            $responseData['status'] = "success";
        }else{
            $responseData['status'] = "error";
            $responseData['message'] = "Fail to get Booked Sites/ No sites Booked.";
        }
        $responseData['list'] = $result;
        echo json_encode($responseData);
        //Closing the statment
        $this->con = null;
        exit();
    }
    public function getContactPersons($request){
        $companyid = mysql_real_escape_string($request->getAttribute('comapnyid'));
        $query = "select email,name,contact_number from contact_persons where company_id = '$companyid'";
        $stmt = $this->con->prepare($query);
        //Executing the statment
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $responseData = array();
        if(count($result)){
            $responseData['status'] = "success";
        }else{
            $responseData['status'] = "error";
            $responseData['message'] = "Fail to get Contact Persons/ No Contact Persons Present.";
        }
        $responseData['list'] = $result;
        echo json_encode($responseData);
        //Closing the statment
        $this->con = null;
        exit();
    }
    public function saveOrders($request){
        $order_id = uniqid('ord_');
        $customer_id = mysql_real_escape_string($request->getParam('customer_id'));
        $sales_person_id = mysql_real_escape_string($request->getParam('sales_person_id'));
        $site_id = mysql_real_escape_string($request->getParam('site_id'));
        $from_date = mysql_real_escape_string($request->getParam('from_date'));         
        $to_date = mysql_real_escape_string($request->getParam('to_date'));
        $custom_cart_rate = mysql_real_escape_string($request->getParam('custom_cart_rate'));
        $order_date = mysql_real_escape_string($request->getParam('order_date'));
        $installation_charges = mysql_real_escape_string($request->getParam('installation_charges'));
        $amount = mysql_real_escape_string($request->getParam('amount'));
        $number_days = date_diff(date_create($to_date),date_create($from_date));
        $order_status = mysql_real_escape_string($request->getParam('order_status'));
        $created_at = date("Y-m-d H:i:s");
        $query = "INSERT INTO order_info(id, customer_id, sales_person_id, site_id, from_date, to_date, `days`,custom_cart_rate,order_date,installation_charges,amount,order_status ,created_at) VALUES ('$order_id', '$customer_id', '$sales_person_id', '$site_id', '$from_date', '$to_date',$number_days->days,$custom_cart_rate,'$order_date',$installation_charges,$amount,$order_status,'$created_at')";

        $stmt = $this->con->prepare($query);
        //Executing the statment
         $result = $stmt->execute();
        if ($result) {
            //Insert success
            $responseData['status'] = "success";
            $responseData['message'] = "Success! Order created";
        } else {
            //Insert fail
            $responseData['status'] = "error";
            $responseData['message'] = "Error! Something went wrong";
        }
        echo json_encode($responseData);
    }
    public function updateOrder($request){
        $order_id = mysql_real_escape_string($request->getParam('order_id'));
        $customer_id = mysql_real_escape_string($request->getParam('customer_id'));
        $sales_person_id = mysql_real_escape_string($request->getParam('sales_person_id'));
        $site_id = mysql_real_escape_string($request->getParam('site_id'));
        $from_date = mysql_real_escape_string($request->getParam('from_date'));         
        $to_date = mysql_real_escape_string($request->getParam('to_date'));
        $custom_cart_rate = mysql_real_escape_string($request->getParam('custom_cart_rate'));
        $order_date = mysql_real_escape_string($request->getParam('order_date'));
        $installation_charges = mysql_real_escape_string($request->getParam('installation_charges'));
        $amount = mysql_real_escape_string($request->getParam('amount'));
        $number_days = date_diff(date_create($to_date),date_create($from_date));
        $order_status = mysql_real_escape_string($request->getParam('order_status'));
        $created_at = date("Y-m-d H:i:s");
        $query = "UPDATE order_info SET customer_id = '".$customer_id."', sales_person_id = '".$sales_person_id."', site_id = '".$site_id."', from_date = '".$from_date."', to_date = '".$to_date."', days = '".$number_days->days."',custom_cart_rate = '".$custom_cart_rate."',order_date = '".$order_date."',installation_charges = '".$installation_charges."',amount = '".$amount."',order_status = '".$order_status."' WHERE id = '".$order_id."' ";
        $stmt = $this->con->prepare($query);
        //Executing the statment
         $result = $stmt->execute();
        if ($result) {
            //Insert success
            $responseData['status'] = "success";
            $responseData['message'] = "Success! Order Updated";
        } else {
            //Insert fail
            $responseData['status'] = "error";
            $responseData['message'] = "Error! Something went wrong";
        }
        echo json_encode($responseData);
    }
    public function saveContactPerson($request){
        $id = uniqid('cp_');
        $name = mysql_real_escape_string($request->getParam('name'));
        $company_id = mysql_real_escape_string($request->getParam('company_id'));
        $contact_number = mysql_real_escape_string($request->getParam('contact_number'));
        $email_id = mysql_real_escape_string($request->getParam('email_id'));
        $created_at = date("Y-m-d H:i:s");
        $checkquery = "SELECT id FROM contact_persons WHERE email = '".$email_id."' ";
        $stmt = $this->con->prepare($checkquery);
        //Executing the statment
        $stmt->execute();
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        
        if($result)
            $query = "UPDATE contact_persons SET name = '".$name."',email = '".$email_id."',company_id = '".$company_id."',contact_number = '".$contact_number."' WHERE id = '".$result['id']."' ";
        else 
            $query = "INSERT INTO contact_persons(id, name, email, company_id, contact_number, created_at) VALUES('$id', '$name', '$email_id', '$company_id', '$contact_number','$created_at')";

        /*echo $query;
        exit();*/
        $stmt = $this->con->prepare($query);
        //Executing the statment
        $result = $stmt->execute();
        if ($result) {
            //Insert success
            $responseData['status'] = "success";
            $responseData['message'] = "Success! Contact Person created";
        } else {
            //Insert fail
            $responseData['status'] = "error";
            $responseData['message'] = "Error! Something went wrong";
        }
        echo json_encode($responseData);
    }
    public function savePO($request){
        $id = uniqid('po_');
        $order_id = mysql_real_escape_string($request->getParam('order_id'));
        $administrator_id = mysql_real_escape_string($request->getParam('administrator_id'));
        $po_number = mysql_real_escape_string($request->getParam('po_number'));
        $img_url = mysql_real_escape_string($request->getParam('img_url'));
        $po_status = mysql_real_escape_string($request->getParam('po_status'));
        $created_at = date("Y-m-d H:i:s");
        $checkquery = "SELECT id FROM po_reveived WHERE order_id = '".$order_id."' ";
        $stmt = $this->con->prepare($checkquery);
        //Executing the statment
        $stmt->execute();
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        
        if($result)
            $query = "UPDATE po_reveived SET po_number = '".$po_number."',img_url = '".$img_url."',po_status = '".$po_status."' WHERE id = '".$result['id']."' ";
        else 
            $query = "INSERT INTO po_reveived(id, order_id, administrator_id, po_number, img_url,po_status, created_at) VALUES('$id', '$order_id', '$administrator_id', '$po_number', '$img_url',$po_status,'$created_at')";

        /*echo $query;
        exit();*/
        $stmt = $this->con->prepare($query);
        //Executing the statment
        $result = $stmt->execute();
        if ($result) {
            //Insert success
            $responseData['status'] = "success";
            $responseData['message'] = "Success! PO created";
        } else {
            //Insert fail
            $responseData['status'] = "error";
            $responseData['message'] = "Error! Something went wrong";
        }
        echo json_encode($responseData);
    }
    public function getAllSalespersons($request){
        $query = "SELECT `id`,CONCAT(`first_name`,' ',`last_name`) fullname , `email_id`, `contact_number`, `address`, `joining_date`, `status` FROM `sales_person`";
        //Crating an statement
        $stmt = $this->con->prepare($query);
        //Executing the statment
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $responseData = array();
        if(count($result)){
            $responseData['status'] = "success";
        }else{
            $responseData['status'] = "error";
            $responseData['message'] = "Fail to get Sales Person List/ Sales Person list not available.";
        }
        $responseData['list'] = $result;
        echo json_encode($responseData);
        //Closing the statment
        $this->con = null;
        exit();

    }
    public function getAllSites($request){
        $query = "SELECT `id`, `location_name`, `site_code`, `size`, `cart_rate`, `min_cart_rate`, `landmark` FROM `site_info`";
        //Crating an statement
        $stmt = $this->con->prepare($query);
        //Executing the statment
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $responseData = array();
        if(count($result)){
            $responseData['status'] = "success";
        }else{
            $responseData['status'] = "error";
            $responseData['message'] = "Fail to get Sites List/ Sites list not available.";
        }
        $responseData['list'] = $result;
        echo json_encode($responseData);
        //Closing the statment
        $this->con = null;
        exit();

    }
    public function getAllOrders($request){
        // 
        $query = "select t1.id orderid,t2.`id` customer_id,t2.party_name customer_name,t3.id sp_id,CONCAT(t3.`first_name`,' ',t3.`last_name`) spfullname,t4.id site_id,t4.location_name,t4.site_code,t5.po_number,t5.img_url  from order_info t1 inner join companies t2 on t1.customer_id = t2.id inner join sales_person t3 on t3.id = t1.sales_person_id inner join site_info t4 on t4.id = t1.site_id left join po_reveived t5 on t4.id = t5.order_id";
        //Crating an statement
        $stmt = $this->con->prepare($query);
        //Executing the statment
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $responseData = array();
        if(count($result))
            $responseData['status'] = "success";
        else
            $responseData['status'] = "fail";
        $responseData['list'] = $result;
        echo json_encode($responseData);
        //Closing the statment
        $this->con = null;
        exit();

    }
    public function loginToRS($request){
        $email_id = $request->getParam('email_id');
        $password = $request->getParam('password');
        $type_of_login = $request->getParam('type_of_login');

        //check use is exist or not with respective type of login
        if($email_id === "" || $type_of_login === "" || $password === ""){
            echo '{"status":"error","message":"Some fields are missing."}';
            return;
        }
        switch ($type_of_login) {
            case 'master_admin':
                $query = "SELECT * from master_admin WHERE email_id = '$email_id'";
                break;
            case 'administrator':
                $query = "SELECT * from administrator WHERE email_id = '$email_id'";
                break;
            case 'sales_person':
                $query = "SELECT * from sales_person WHERE email_id = '$email_id'";
                break;
            }
        $stmt = $this->con->query($query);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $num_rows = count($result);
        if($num_rows > 0){//user exist
            //printf($result);
            $hashed_password = $result[0]['password'];
            if (password_verify($password, $hashed_password)){                         
                echo '{"status":"success","message":"login Successfully","detail":'.json_encode($result[0]).'}';            
            }else{
                echo '{"status":"error","message":"Please check email_id and password."}';
            }
        }else{//user is not exits
            //printf($result);
            echo '{"status":"error","message":"email_id not Registered with us."}';
        }
    }
    
}
