<?php
header('Access-Control-Allow-Origin: *');
require '../vendor/autoload.php';
require_once '../include/DbOperation.php';

$app = new Slim\App();

$app->get('/hello/{name}', function ($request, $response, $args) {
    $response->write("Hello, " . $args['name']);
    return $response;
});


//Create New MasterAdmin
$app->post('/create/master_admin', function ($request, $response, $args) {
    $db = new DbOperation();
    $res = $db->createMasterAdmin($request);
});

//Create New Administrator
$app->post('/create/administrator', function ($request, $response, $args) {
    $db = new DbOperation();
    $res = $db->createAdministrator($request);
});

//Create New SalesPerson
$app->post('/create/sales_person', function ($request, $response, $args) {
    $db = new DbOperation();
    $res = $db->createSalesPerson($request);
});

//Create New Customer
$app->post('/companies/create', function ($request, $response, $args) {
    $db = new DbOperation();
    $res = $db->createCompany($request);
});

//Create New Site
$app->post('/sites/create', function ($request, $response, $args) {
    $db = new DbOperation();
    $res = $db->createSite($request);
});

//Login handler
$app->post('/login', function ($request, $response, $args) {
    $db = new DbOperation();
    $res = $db->loginToRS($request);
});

//All Customers handler
$app->get('/companies', function ($request, $response, $args) {
    $db = new DbOperation();
    $res = $db->getAllCompanies($request);
});

//All SalesPerson handler
$app->get('/salespersons', function ($request, $response, $args) {
    $db = new DbOperation();
    $res = $db->getAllSalespersons($request);
});

//All Orders handler
$app->get('/orders', function ($request, $response, $args) {
    $db = new DbOperation();
    $res = $db->getAllOrders($request);
});

//All Sites handler
$app->get('/sites', function ($request, $response, $args) {
    $db = new DbOperation();
    $res = $db->getAllSites($request);
});
//save orders 
$app->post('/orders/create', function ($request, $response, $args) {
    $db = new DbOperation();
    $res = $db->saveOrders($request);
});

//udpate order
$app->post('/orders/update', function ($request, $response, $args) {
    $db = new DbOperation();
    $res = $db->updateOrder($request);
});

//save po 
$app->post('/po/create', function ($request, $response, $args) {
    $db = new DbOperation();
    $res = $db->savePO($request);
});

//save po 
$app->post('/contactpersons/create', function ($request, $response, $args) {
    $db = new DbOperation();
    $res = $db->saveContactPerson($request);
});

// fetch all orders with no po
$app->get('/orders/pendingpo', function ($request, $response, $args) {
    $db = new DbOperation();
    $res = $db->getPendingPOorders($request);
});

// fetch all sites available for selected date
$app->get('/sites/available/{date}', function ($request, $response, $args) {
    $db = new DbOperation();
    $res = $db->getAvailableSites($request);
});
// fetch all sites booked for selected date
$app->get('/sites/booked/{date}', function ($request, $response, $args) {
    $db = new DbOperation();
    $res = $db->getBookedSites($request);
});
/*$app->post('/createnew', function ($request, $response, $args) {
    getParam('username');

});
*/
// fetch all contact persons by company
$app->get('/contactpersons/{comapnyid}', function ($request, $response, $args) {
    $db = new DbOperation();
    $res = $db->getContactPersons($request);
});
$app->run();	
