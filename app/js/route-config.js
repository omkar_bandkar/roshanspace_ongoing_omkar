var mainApp = angular.module("mainApp", ['ui.router','ngCookies','selectize','daterangepicker','ngFileUpload']);

//JS variable
var messages = {
  loginParamError:"All fields are required",

}


/*
mainApp.config(['$routeProvider', function($routeProvider) {
$routeProvider.when('/', {
templateUrl: 'app/template/login.html',
controller: 'loginController'
}).when('/master_admin', {
templateUrl: 'app/template/ma_dashboard.html',
controller: 'masterAdminController'
}).when('/administrator', {
templateUrl: 'app/template/admin_dashboard.html',
controller: 'administratorController'
}).when('/sales_person', {
templateUrl: 'app/template/sp_dashboard.html',
controller: 'salesPersonController'
}).when('/administrator_add', {
templateUrl: 'app/template/admin-registration.html',
controller: 'administratorAddController'
}).when('/sales_person_add', {
templateUrl: 'app/template/salesperson-registration.html',
controller: 'salespersonAddController'
}).when('/customer_add', {
templateUrl: 'app/template/customer-registration.html',
controller: 'customerAddController'
}).
otherwise({
redirectTo: '/'
});
}]);


*/
mainApp.config(['$stateProvider',function($stateProvider) {
  $stateProvider.state('login', {
    url: '/',
    templateUrl: 'app/template/login.html',
    controller: 'loginController'
  }).state('master_admin', {
    url:'/master_admin',
    templateUrl: 'app/template/ma_dashboard.html',
    controller: 'masterAdminController'
  }).state('administrator', {
    url:'/administrator',
    templateUrl: 'app/template/admin_dashboard.html',
    controller: 'administratorController'
  }).state('sales_person', {
    url:'/sales_person',
    templateUrl: 'app/template/sp_dashboard.html',
    controller: 'salesPersonController'
  }).state('administrator_add', {
    url:'/administrator_add',
    templateUrl: 'app/template/admin-registration.html',
    controller: 'administratorAddController'
  }).state('sales_person_add', {
    url:'/sales_person_add',
    templateUrl: 'app/template/salesperson-registration.html',
    controller: 'salespersonAddController'
  }).state('customer_add', {
    url:'/customer_add',
    templateUrl: 'app/template/customer-registration.html',
    controller: 'customerAddController'
  }).state('order', {
    url:'/order',
     resolve: {            
            customers: function($http){
                return $http.get(url.getcustomers)
                .then(function(response){
                  return response.data.list;
                })
                },
            salesPersons: function($http){
                return $http.get(url.getsalespersons)
                .then(function(response){
                  return response.data.list;
                })
                },
            sites: function($http){
                return $http.get(url.getsitelists)
                .then(function(response){
                  return response.data.list;
                })
                },
        },
    templateUrl: 'app/template/create-order.html',
    controller: 'orderController'
  }).state('po', {
    url:'/po',
     resolve: {            
            pending_po_order: function($http){
                return $http.get(url.getpendingpoorder)
                .then(function(response){
                  return response.data.list;
                })
                },
        },
    templateUrl: 'app/template/create-po.html',
    controller: 'poController'
  })
}]);
