//localhost/ipaddress
var host = "http://localhost/";

var ip = {
	api: host+"roshanspace_ongoing/api/v1/"
};

var url = {
	login:ip.api+"login",
	createadmin:ip.api+"create/administrator",
	createsp:ip.api+"create/sales_person",
	createCompanies:ip.api+"companies/create",
	getcustomers:ip.api+"companies",
	getsalespersons:ip.api+"salespersons",
	getsitelists:ip.api+"sites",
	createsite:ip.api+"sites/create",
	createorder:ip.api+"orders/create",
	getpendingpoorder:ip.api+"orders/pendingpo",
	file_upload_url:ip+"roshanspace_ongoing/api/upload.php",
	createpo:ip.api+"po/create",
	getAllOrders:ip.api+"orders",
	getAllCompanies:ip.api+"companies",
	getAllSites:ip.api+"sites"
};