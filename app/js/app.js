//Services
mainApp.service('service1', function($http, $rootScope, $cookieStore, $location, $state,$q) {
  this.showMessage = function(status, msg){
    $("#msg1").html(status);
    $("#msg2").html(msg);    
    $('#messages').modal('show');
    setTimeout(function() {$('#messages').modal('hide');}, 5000);
  };
  //data handler according to role
  this.fetchData = function(role){
    var dashboardData = {};
    var defer;
    defer = $q.defer();
    $q.all([
    $http({url: url.getAllCompanies,method: 'GET'}).then(function(response) {
      console.log(response);
      if(response.data.status === 'success'){
        dashboardData.companyData = response.data.list;
      } else {
        dashboardData.companyData = new Array();
      }
    }),
    $http({url: url.getAllSites,method: 'GET'}).then(function(response) {
      if(response.data.status === 'success'){
        dashboardData.siteData = response.data.list;
      } else {
        dashboardData.siteData = new Array();
      }
    }),
    $http({url: url.getAllOrders,method: 'GET'}).then(function(response) {
      console.log(response);
      if(response.data.status === 'success'){
        dashboardData.ordersData = response.data.list;
      } else {
        dashboardData.ordersData = new Array();
      }
    }),
    ]).then(function() {
      defer.resolve(dashboardData);
    });
    return defer.promise;
    //return dashboardData;
  }
  //login handler
  this.loginHandler = function(detail, type_of_login){
    $cookieStore.put('user_email',detail.email);
    $cookieStore.put('loggedIn', 'true');
    $cookieStore.put('userRole', type_of_login);
    $cookieStore.put('userDetail',detail);
    $rootScope.fullName = detail.first_name+" "+detail.last_name;
    $rootScope.userRole = type_of_login;
    $rootScope.loggedIn = true;
    $("#wrapper").removeClass("removeMenuPadding");
    switch(type_of_login){
      case "master_admin":
        $state.go('master_admin');
        break;
      case "administrator":
        $state.go('administrator');
        break;
      case "sales_person":
        $state.go('sales_person');
        break;        
      default:
        alert("default redirect");  
      }
  };

//this will check user session is it expired or not
  this.checkSession = function(){
    if($cookieStore.get('loggedIn')){
    var detail = $cookieStore.get('userDetail');
    $rootScope.id = detail.id;
    $rootScope.fullName = detail.first_name+" "+detail.last_name;
    $rootScope.email = detail.email;
    $rootScope.userRole = $cookieStore.get('userRole');
    $rootScope.loggedIn = true;
    }else{
      //this.showMessage("Timeout", "Your Session is expired please Login Again.")
      $state.go('login');
    }
    
  };

  //logout handler
  this.logoutHandler = function(){
    $("#loading").show();
    $rootScope.email = "";
    $rootScope.userRole = "";
    $rootScope.loggedIn = false;
    //clear all cookies storage
    $cookieStore.remove('loggedIn');
    $cookieStore.remove('userDetail');
    $cookieStore.remove('userRole');
    $("#loading").hide();
    //$location.path("/");
    $state.go('login');
  };

  this.redirectHandler = function(){
    if($cookieStore.get('loggedIn')){
      switch($cookieStore.get('userRole')){
        case "master_admin":
        $state.go('master_admin');
        break;
      case "administrator":
        $state.go('administrator');
        break;
      case "sales_person":
        $state.go('sales_person');
        break;        
      default:
        alert("default redirect"); 
        }      
      }
    };

});

//loginController
mainApp.controller('loginController', function ($scope, $stateParams, $http, service1, $location, $cookieStore, $rootScope, $state) {
$scope.viewReady = false;
//service1.redirectHandler();  
//remove menu padding on login page
$("#wrapper").addClass("removeMenuPadding");
$("#loading").hide();
$scope.viewReady = true
  $scope.loginMe = function(){
    $("#loading").show();
    var email_id = $("#email").val(); 
    var password = $("#pass").val(); 
    var type_of_login = $('input[name=loginas]:checked', '#login_form').val();

    /*scope.$watch('name', function(newValue, oldValue) {
    scope.counter = scope.counter + 1;
    });*/
    if(email_id === "" || password === "" || type_of_login === ""){
      service1.showMessage("error", messages.loginParamError);
      $("#loading").hide();
      return;      
    }else{
      var parameter = "type_of_login="+type_of_login+"&email_id="+email_id+"&password="+password;
      $http({
          url: url.login,
          method: 'POST',
          data: parameter,
          headers: {
              "Content-Type": "application/x-www-form-urlencoded"
          }

      }).success(function(data){
          console.log(data);
          $("#loading").hide();
          var status = data.status;
          if(status === "success"){//login successful
            service1.showMessage(status, data.message);            
            service1.loginHandler(data.detail, type_of_login);
          }else{
            $("#loading").hide();
            service1.showMessage(status, data.message);
          }
      }).error(function(error){
        $("#loading").hide();
          service1.showMessage("error", "Fail to connect.");
      });            
    }
  };


});


mainApp.controller('navController', function ($scope, $stateParams, $http, service1, $location, $cookieStore, $rootScope, $state) {
  service1.checkSession();

//logout clicked
  $scope.logout  = function(){
    var r = confirm(" you really want to logout? ");
      if (r === true) {
        service1.logoutHandler();
      }
  };
  //Menu Handler


});



mainApp.controller('masterAdminController', function ($scope, $stateParams, $http, service1, $location, $cookieStore, $rootScope, $state) {
  service1.checkSession();
  $scope.message = "masterAdmindashboard";
  var allrequiredData = service1.fetchData();
  allrequiredData.then(function(allData){
    $scope.companyData = allData.companyData;
    $scope.ordersData = allData.ordersData;
    $scope.siteData = allData.siteData;
  });
});


/*ADD OPERATIUON START*/
mainApp.controller('administratorAddController', function ($scope, $stateParams, $http, service1, $location, $cookieStore, $rootScope, $state) {
  service1.checkSession();
  $scope.form_header = "Administrator Registration";
  $scope.form_type = "administrator";
  var allrequiredData = service1.fetchData();
  allrequiredData.then(function(allData){
    $scope.companyData = allData.companyData;
    $scope.ordersData = allData.ordersData;
    $scope.siteData = allData.siteData;
  });
  $scope.registerAdmin = function(){
    $("#loading").show();
    var parameter = "first_name="+$scope.firstname+"&last_name="+$scope.lastname+"&email_id="+$scope.email+"&password="+$scope.user_password+"&contact_number="+$scope.contact+"&address="+$scope.address+"&status="+$("#status").val();
    console.log(parameter);
      $http({
          url: url.createadmin,
          method: 'POST',
          data: parameter,
          headers: {
              "Content-Type": "application/x-www-form-urlencoded"
          }

      }).success(function(data){
          console.log(data);
          $("#loading").hide();
          var status = data.status;
          if(status === "success"){
            service1.showMessage(status, data.message);
            $state.reload();
          }else{
            service1.showMessage(status, data.message);
          }
      }).error(function(error){
        $("#loading").hide();
          service1.showMessage("error", "Fail to connect.");
      });      

  }



});

mainApp.controller('salespersonAddController', function ($scope, $stateParams, $http, service1, $location, $cookieStore, $rootScope, $state) {
  service1.checkSession();
  $scope.form_header = "Sales Person Registration";
  $scope.form_type = "sales_person";
  var allrequiredData = service1.fetchData();
  allrequiredData.then(function(allData){
    $scope.companyData = allData.companyData;
    $scope.siteData = allData.siteData;
  });
  $scope.registerSalesPerson = function(){
    $("#loading").show();
    var parameter = "first_name="+$scope.firstname+"&last_name="+$scope.lastname+"&email_id="+$scope.email+"&password="+$scope.user_password+"&contact_number="+$scope.contact+"&address="+$scope.address+"&status="+$("#status").val();
    console.log(parameter);
      $http({
          url: url.createsp,
          method: 'POST',
          data: parameter,
          headers: {
              "Content-Type": "application/x-www-form-urlencoded"
          }

      }).success(function(data){
          console.log(data);
          $("#loading").hide();
          var status = data.status;
          if(status === "success"){
            service1.showMessage(status, data.message);
            $state.reload();
          }else{
            service1.showMessage(status, data.message);
          }
      }).error(function(error){
        $("#loading").hide();
          service1.showMessage("error", "Fail to connect.");
      });
    }
});

mainApp.controller('customerAddController', function ($scope, $stateParams, $http, service1, $location, $cookieStore, $rootScope, $state) {
  service1.checkSession();  

  $scope.registerCustomer = function(){
    $("#loading").show();
    var parameter = "partyname="+$scope.partyname+"&address="+$scope.address+"&email_id="+$scope.useremail+"&ofcnumber="+$scope.ofcnumber+"&othernumber="+$scope.othernumber+"&pannumber="+$scope.pannumber;
    console.log(parameter);
      $http({
          url: url.createCompanies,
          method: 'POST',
          data: parameter,
          headers: {
              "Content-Type": "application/x-www-form-urlencoded"
          }

      }).success(function(data){
          console.log(data);
          $("#loading").hide();
          var status = data.status;
          if(status === "success"){
            service1.showMessage(status, data.message);
            $state.reload();
          }else{
            service1.showMessage(status, data.message);
          }
      }).error(function(error){
        $("#loading").hide();
          service1.showMessage("error", "Fail to connect.");
      });
  };

});
/*ADD OPERATION END*/

/*Custom Directive*/
mainApp.directive('customSearch', function(){

  return {
    templateUrl:"app/template/search-fields.html"
  };

});


mainApp.directive('registrationForm', function(){
  return {
    templateUrl:"app/template/registration-form.html"
  }

});


mainApp.controller('administratorController', function ($scope, $stateParams, $http, service1, $location, $cookieStore, $rootScope, $state) {
  service1.checkSession();
  $scope.message = "administratordashboard";


});

mainApp.controller('salesPersonController', function ($scope, $stateParams, $http, service1, $location, $cookieStore, $rootScope, $state) {
  service1.checkSession();
  $scope.message = "salesPersondashboard";


});

//PO Controller
mainApp.controller('poController', function ($scope, $stateParams, $http, service1, $location, $cookieStore, $rootScope, $state, pending_po_order, Upload, $timeout) {
  service1.checkSession();

  $scope.pending_po_order = pending_po_order;

  $scope.ordersConfig = {
    maxItems: 1,
    valueField: 'orderid',
    labelField: 'orderid',
    searchField: 'orderid'
    };

    $scope.uploadFiles = function(file, errFiles) {
      $scope.f = file;
      $scope.errFile = errFiles && errFiles[0];
      if (file) {
        file.upload = Upload.upload({
          url: "http://localhost/roshanspace_ongoing/api/upload.php",
          data: {fileToUpload: file}
        });

        file.upload.then(function (response) {
          $timeout(function () {
            file.result = response.data.message;
            if(response.data.status === "success"){
              $("#uploaded_file_path").val(response.data.file_path);
            }           
          });
        }, function (response) {
            if(response.data.status === "success"){
              $("#uploaded_file_path").val(response.data.file_path);
            } 
          /*if (response.status > 0)
            $scope.errorMsg = response.status + ': ' + response.message;*/

        }, function (evt) {
          file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
        });
      }   
    }


    $scope.addPO = function(){
      $("#loading").show();
      if($scope.selectedOrder === undefined){
        $("#loading").hide();
        service1.showMessage("error", "Select Order.");
        return;
      }
      var parameter = "order_id="+$scope.selectedOrder+"&administrator_id="+$rootScope.id+"&po_number="+$scope.po_number+"&img_url="+$("#uploaded_file_path").val()+"&po_status="+$("#postatus").val();

      $http({
          url: url.createpo,
          method: 'POST',
          data: parameter,
          headers: {
              "Content-Type": "application/x-www-form-urlencoded"
          }
      }).success(function(data){
          console.log(data);
          $("#loading").hide();
          var status = data.status;
          if(status === "success"){
            service1.showMessage(status, data.message);
            //$state.reload();
          }else{
            service1.showMessage(status, data.message);
          }
      }).error(function(error){
        $("#loading").hide();
          service1.showMessage("error", "Fail to connect.");
      });


    }

});

mainApp.controller('orderController', function ($scope, $stateParams, $http, service1, $location, $cookieStore, $rootScope, $state, customers, salesPersons, sites) {
  service1.checkSession();

  $("#loading").show();
  $scope.customers = customers;
  $scope.salespersons = salesPersons;
  $scope.sites = sites;
  $scope.totalDays = 0;
  $scope.date_selected = false;
  $scope.spId = "";
  console.log(salesPersons)
  $scope.customerConfig = {
    maxItems: 1,
    valueField: 'id',
    labelField: 'party_name',
    searchField: 'party_name'
    };  
  $scope.siteConfig = {
    maxItems: 1,
    valueField: 'id',
    labelField: 'location_name',
    searchField: 'location_name'
    };  
  $scope.spConfig = {
    maxItems: 1,
    valueField: 'id',
    labelField: 'fullname',
    searchField: 'fullname'
    };    
  $scope.datePicker = {};
  //startDate and endDate are moment objects so you need to convert those to strings while passing to the api
  $scope.datePicker.dateRange = {startDate: null, endDate: null};
  $scope.setValue = function(){
    if($scope.datePicker.dateRange.startDate && $scope.datePicker.dateRange.endDate){
        $scope.totalDays = $scope.datePicker.dateRange.endDate.diff($scope.datePicker.dateRange.startDate,'days');
        //console.log($scope.datePicker.dateRange.startDate.format('YYYY-MM-DD'));
        //console.log($scope.datePicker.dateRange.endDate.format('YYYY-MM-DD'));
        $scope.date_selected = true;
    }
  };
  
  $scope.datePicker.orderdate = moment();
  //$scope.datePicker.orderdate = {};
 if($rootScope.userRole === "sales_person"){
      $scope.selectedSP = $rootScope.id;
    }
  
  $("#loading").hide();

  $scope.placeOrder = function(){
    $("#loading").show();
    
    $scope.check_prices = $scope.installation_charges === undefined || $scope.amount === undefined;
    if($scope.check_prices){
      service1.showMessage("error", "Enter correct prices format");
      $("#loading").hide();
      return;
    }
    if($scope.selectedCustomer === undefined) { service1.showMessage("error", "Select Customer"); $("#loading").hide(); return;}
    if($scope.selectedSP === undefined && $rootScope.userRole !== "sales_person") { service1.showMessage("error", "Select SalesPerson"); $("#loading").hide(); return;}
    if($scope.selectedSite === undefined) { service1.showMessage("error", "Select Site"); $("#loading").hide(); return;}

      
    console.log($scope.datePicker.orderdate);
    console.log($scope.datePicker.orderdate.format('YYYY-MM-DD'));
    var parameter = "customer_id="+$scope.selectedCustomer+"&sales_person_id="+$scope.selectedSP+"&site_id="+$scope.selectedSite+"&from_date="+$scope.datePicker.dateRange.startDate.format('YYYY-MM-DD')+"&to_date="+$scope.datePicker.dateRange.endDate.format('YYYY-MM-DD')+"&custom_cart_rate="+$scope.cust_cart_rate+"&order_date="+$scope.datePicker.orderdate.format('YYYY-MM-DD')+"&installation_charges="+$scope.installation_charges+"&amount="+$scope.amount+"&order_status="+$("#order_status").val();

    console.log(parameter);

    $http({
          url: url.createorder,
          method: 'POST',
          data: parameter,
          headers: {
              "Content-Type": "application/x-www-form-urlencoded"
          }
      }).success(function(data){
          console.log(data);
          $("#loading").hide();
          var status = data.status;
          if(status === "success"){
            service1.showMessage(status, data.message);
            //$state.reload();
          }else{
            service1.showMessage(status, data.message);
          }
      }).error(function(error){
        $("#loading").hide();
          service1.showMessage("error", "Fail to connect.");
      });

  };





    //config datepicker
    /*$('#dateRangeSelected').daterangepicker({
      autoUpdateInput: false,
      locale: {
        cancelLabel: 'Clear',
        format: 'YYYY-MM-DD'
      }
    },
    function(start, end, label) {
        $scope.totalDays = end.diff(start,'days');
        console.log(end.diff(start,'days'));
        console.log($scope.totalDays);
    });

    $('#dateRangeSelected').on('apply.daterangepicker', function(ev, picker) {
      $(this).val(picker.startDate.format('YYYY-MM-DD') + ' - ' + picker.endDate.format('YYYY-MM-DD'));
    });

    $('#dateRangeSelected').on('cancel.daterangepicker', function(ev, picker) {
      $(this).val('');
    });*/
});