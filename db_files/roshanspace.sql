-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 05, 2016 at 06:47 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `roshanspace`
--

-- --------------------------------------------------------

--
-- Table structure for table `administrator`
--

CREATE TABLE `administrator` (
  `id` varchar(45) NOT NULL,
  `first_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `email_id` varchar(110) NOT NULL,
  `password` varchar(124) NOT NULL,
  `contact_number` varchar(45) DEFAULT NULL,
  `address` text,
  `status` tinyint(1) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `last_login` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `administrator`
--

INSERT INTO `administrator` (`id`, `first_name`, `last_name`, `email_id`, `password`, `contact_number`, `address`, `status`, `created_at`, `last_login`) VALUES
('ad_14534', 'Jane', 'Doe', 'janedoe@example.com', 'testplanepass', '9800452122', 'test street,doesnot exists address', 1, '2016-09-20 16:20:23', NULL),
('ad_57f2039b24116', 'Test', 'User', 'testuser@gmail.com', '$2y$10$hs0SlXmOmeN.r1Ejra6LzezJhh.MBWbRKDN2q9lOdvJwopXzG/Qgy', '9869286465', 'test', 0, '2016-10-03 09:07:07', NULL),
('ad_57f20411af965', 'Test', 'User', 'testuser1@gmail.com', '$2y$10$wdE5uEhkAH8QYSHNcZX5q.zLfjfa.OHi7DHuXZBy2u2INVLZPmNEe', '9869286465', 'test user', 0, '2016-10-03 09:09:05', NULL),
('ad_57f521c9282f8', 'test', 'admin', 'ad@roshanspace.com', '$2y$10$E162pO/zOA4fHbku/GyX/uDxM7Gx2nwbzcaJT.FDLIVnNnfBh.Z5C', '9096225588', 'Somewhere', 0, '2016-10-05 17:52:41', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `customer_info`
--

CREATE TABLE `customer_info` (
  `id` varchar(45) NOT NULL,
  `party_name` varchar(110) DEFAULT NULL,
  `address` text,
  `office_number` varchar(45) DEFAULT NULL,
  `other_number` varchar(45) DEFAULT NULL,
  `PAN_number` varchar(45) DEFAULT NULL,
  `contact_person_name` varchar(110) DEFAULT NULL,
  `email_id` varchar(110) DEFAULT NULL,
  `contact_person_number` varchar(45) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customer_info`
--

INSERT INTO `customer_info` (`id`, `party_name`, `address`, `office_number`, `other_number`, `PAN_number`, `contact_person_name`, `email_id`, `contact_person_number`, `created_at`) VALUES
('ci_14532', 'Test Party1', 'test street,doesnot exists address', '0226523604', '02226524291', 'AYVXZ6254T', 'washington sundar', 'janedoe@example.com', '9800452122', '2016-09-20 16:35:04');

-- --------------------------------------------------------

--
-- Table structure for table `master_admin`
--

CREATE TABLE `master_admin` (
  `id` varchar(45) NOT NULL,
  `first_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `email_id` varchar(110) DEFAULT NULL,
  `password` varchar(124) DEFAULT NULL,
  `contact_number` varchar(45) DEFAULT NULL,
  `address` text,
  `created_at` datetime DEFAULT NULL,
  `last_login` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `master_admin`
--

INSERT INTO `master_admin` (`id`, `first_name`, `last_name`, `email_id`, `password`, `contact_number`, `address`, `created_at`, `last_login`) VALUES
('ma_57ee0ca5c0c09', 'john', 'doe', 'johndoe@example.com', '$2y$10$0W6wmQL7lfUaAsWAh8Q22.PfpD8hTUeufzde6vsMWgh0XVGaJaTfq', '9869065212', 'test address here', '2016-09-30 08:56:37', NULL),
('ma_57f52191b48a7', 'test', 'master', 'ma@roshanspace.com', '$2y$10$t9EZ1Kl.boujWorG.GFBx.Jy8fMGLyyl2i8v7HJgeYMN1jyIVUiq.', '9096228844', 'Somewhere in middle', '2016-10-05 17:51:45', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `order_info`
--

CREATE TABLE `order_info` (
  `id` varchar(45) NOT NULL,
  `customer_id` varchar(45) DEFAULT NULL,
  `sales_person_id` varchar(45) DEFAULT NULL,
  `site_id` varchar(110) NOT NULL,
  `from_date` date DEFAULT NULL,
  `to_date` date DEFAULT NULL,
  `days` int(11) DEFAULT NULL,
  `custom_cart_rate` varchar(45) DEFAULT NULL,
  `order_date` date DEFAULT NULL,
  `installation_charges` varchar(45) DEFAULT NULL,
  `amount` varchar(110) DEFAULT NULL,
  `order_status` tinyint(1) DEFAULT NULL,
  `po_status` tinyint(1) DEFAULT NULL,
  `img_url` varchar(512) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `order_info`
--

INSERT INTO `order_info` (`id`, `customer_id`, `sales_person_id`, `site_id`, `from_date`, `to_date`, `days`, `custom_cart_rate`, `order_date`, `installation_charges`, `amount`, `order_status`, `po_status`, `img_url`, `created_at`) VALUES
('ord_57ee42fb07d24', 'ci_14532', 'sp_14536', 'st_123256', '2016-09-30', '2016-10-06', 6, '60000', '2016-10-08', '80000', '75000', 0, NULL, NULL, '2016-09-30 12:48:27'),
('ord_57f48e15bc917', 'ci_14532', 'sp_14536', 'st_57f35f428c453', '2016-09-30', '2016-10-06', 6, '60000', '2016-10-08', '80000', '75000', 0, NULL, NULL, '2016-10-05 07:22:29');

-- --------------------------------------------------------

--
-- Table structure for table `po_reveived`
--

CREATE TABLE `po_reveived` (
  `id` varchar(45) NOT NULL,
  `order_id` varchar(45) NOT NULL,
  `administrator_id` varchar(45) DEFAULT NULL,
  `po_number` varchar(45) NOT NULL,
  `img_url` varchar(512) NOT NULL,
  `created_at` date DEFAULT NULL,
  `po_status` varchar(45) DEFAULT NULL COMMENT 'short/excess'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `po_reveived`
--

INSERT INTO `po_reveived` (`id`, `order_id`, `administrator_id`, `po_number`, `img_url`, `created_at`, `po_status`) VALUES
('po_57ee536ec2b1a', 'ord_57ee42fb07d24', 'ad_14534', 'abcd123', 'testurl123', '2016-09-30', '0');

-- --------------------------------------------------------

--
-- Table structure for table `sales_person`
--

CREATE TABLE `sales_person` (
  `id` varchar(45) NOT NULL,
  `first_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `email_id` varchar(110) DEFAULT NULL,
  `password` varchar(120) DEFAULT NULL,
  `contact_number` varchar(45) DEFAULT NULL,
  `address` text,
  `joining_date` date DEFAULT NULL,
  `leaving_date` date DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `last_login` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sales_person`
--

INSERT INTO `sales_person` (`id`, `first_name`, `last_name`, `email_id`, `password`, `contact_number`, `address`, `joining_date`, `leaving_date`, `status`, `created_at`, `last_login`) VALUES
('sp_14536', 'Jane', 'Doe', 'janedoe@example.com', 'testplanepass', '9800452122', 'test street,doesnot exists address', '2016-08-25', NULL, 1, '2016-09-20 16:34:33', NULL),
('sp_57f36710c5559', 'Test', 'User', 'omkar.bandkar@gmail.com', '$2y$10$yz2jl.HsTPNr8gquoZBrbuo09Qi84GUcrAqFz9pNAjWA4vM.fT0ki', '1234567891', 'hjkhkijkh', '0000-00-00', '0000-00-00', 0, '2016-10-04 10:23:44', NULL),
('sp_57f5226e1b6f0', 'test', 'salesperson', 'sp@roshan', '$2y$10$u1ak/iiQXL3iO8FAN1w3RuIRKGKDNDwIfZYp3rgwAk7.eAN4Yhzm2', '9596886655', 'Don''t Know', '0000-00-00', '0000-00-00', 0, '2016-10-05 17:55:26', NULL),
('sp_57f522a7ab9c7', 'test', 'sp', 'sp@roshanspace.com', '$2y$10$9c91UlzfMW41hHfDqeANeeDHUBD.v7Ycz4.Q7xCZ.QKG6vb1vG17u', '9096225588', 'asdf.f address typing', '0000-00-00', '0000-00-00', 0, '2016-10-05 17:56:23', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `site_info`
--

CREATE TABLE `site_info` (
  `id` varchar(110) NOT NULL,
  `location_name` varchar(110) DEFAULT NULL,
  `site_code` varchar(45) DEFAULT NULL,
  `size` varchar(45) DEFAULT NULL,
  `cart_rate` varchar(45) DEFAULT NULL,
  `min_cart_rate` varchar(45) DEFAULT NULL,
  `landmark` text,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `site_info`
--

INSERT INTO `site_info` (`id`, `location_name`, `site_code`, `size`, `cart_rate`, `min_cart_rate`, `landmark`, `created_at`) VALUES
('st_123256', 'Thane', 'TN001', 'LARGE', '50000', '40000', 'Stn Road', '2016-09-20 16:39:46'),
('st_57f35f428c453', 'Kunj Vihar', 'TN002', 'LARGE', '50000', '40000', 'Kunj Vihar', '2016-10-04 09:50:26'),
('st_57f360b5ccc0e', 'Kunj Vihar', 'TN003', 'LARGE', '50000', '40000', 'Kunj Vihar', '2016-10-04 09:56:37');

-- --------------------------------------------------------

--
-- Table structure for table `tax_info`
--

CREATE TABLE `tax_info` (
  `id` int(11) NOT NULL,
  `tax_type` varchar(110) DEFAULT NULL,
  `amount_in_percent` int(11) DEFAULT NULL,
  `description` text,
  `status` tinyint(1) DEFAULT '0',
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `administrator`
--
ALTER TABLE `administrator`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_info`
--
ALTER TABLE `customer_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_admin`
--
ALTER TABLE `master_admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_info`
--
ALTER TABLE `order_info`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_order_info_customer_id_idx` (`customer_id`),
  ADD KEY `fk_order_info_sales_person_id_idx` (`sales_person_id`),
  ADD KEY `fk_order_info_site_id_idx` (`site_id`);

--
-- Indexes for table `po_reveived`
--
ALTER TABLE `po_reveived`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_po_reveived_order_id_idx` (`order_id`),
  ADD KEY `fk_po_reveived_administrator_id_idx` (`administrator_id`);

--
-- Indexes for table `sales_person`
--
ALTER TABLE `sales_person`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `site_info`
--
ALTER TABLE `site_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tax_info`
--
ALTER TABLE `tax_info`
  ADD PRIMARY KEY (`id`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `order_info`
--
ALTER TABLE `order_info`
  ADD CONSTRAINT `fk_order_info_customer_id` FOREIGN KEY (`customer_id`) REFERENCES `customer_info` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_order_info_sales_person_id` FOREIGN KEY (`sales_person_id`) REFERENCES `sales_person` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_order_info_site_id` FOREIGN KEY (`site_id`) REFERENCES `site_info` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `po_reveived`
--
ALTER TABLE `po_reveived`
  ADD CONSTRAINT `fk_po_reveived_administrator_id` FOREIGN KEY (`administrator_id`) REFERENCES `administrator` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_po_reveived_order_id` FOREIGN KEY (`order_id`) REFERENCES `order_info` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
